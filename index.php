<!DOCTYPE html>
<html>
<head>
	<title>Sé libre</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/materialize.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="192x192" href="favicon/android-chrome-192x192.png">
	<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
	<link rel="manifest" href="favicon/site.webmanifest">
	<link rel="mask-icon" href="favicon/safari-pinned-tab.svg" color="#591876">
	<meta name="apple-mobile-web-app-title" content="Se libre">
	<meta name="application-name" content="Se libre">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="favicon/mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
</head>
<body onload="OnStart();" class="fondo" id="cuerpo">

	<div id="preloader">
		<div class="preloader-wrapper big active" style="position: absolute; left: 45%; top: 40%;">
		<div class="spinner-layer spinner-blue">
			<div class="circle-clipper left">
			<div class="circle"></div>
			</div><div class="gap-patch">
			<div class="circle"></div>
			</div><div class="circle-clipper right">
			<div class="circle"></div>
			</div>
		</div>

		<div class="spinner-layer spinner-red">
			<div class="circle-clipper left">
			<div class="circle"></div>
			</div><div class="gap-patch">
			<div class="circle"></div>
			</div><div class="circle-clipper right">
			<div class="circle"></div>
			</div>
		</div>

		<div class="spinner-layer spinner-yellow">
			<div class="circle-clipper left">
			<div class="circle"></div>
			</div><div class="gap-patch">
			<div class="circle"></div>
			</div><div class="circle-clipper right">
			<div class="circle"></div>
			</div>
		</div>

		<div class="spinner-layer spinner-green">
			<div class="circle-clipper left">
			<div class="circle"></div>
			</div><div class="gap-patch">
			<div class="circle"></div>
			</div><div class="circle-clipper right">
			<div class="circle"></div>
			</div>
		</div>
		</div>
	</div>

	<div id="bienvenida" style="display:none;">
		Bienvenida
	</div>

	<div id="login" style="display:none;">		
		<section id="login" class="card">
			<img src="Recursos/Caballo_vectorized_color.png" alt="" srcset="">
			<form>			
				<div class="input-field col s6">
					<i class="material-icons prefix">account_circle</i>
					<input type="text" id="Lusername" name="Lusername">
					<label for="Lusername">Alias</label>
				</div>
				<div class="input-field col s6">
					<i class="material-icons prefix">vpn_key</i>
					<input type="password" id="Lpass" name="Lpass">
					<label for="Lpass">Contraseña</label>
				</div>			
			</form>
			<br>
			¿No tienes una cuenta? <a href="#" onClick="registro();">Registrate</a>
			<br>
			<center><button onclick="Login();" class="waves-effect waves-light btn purple">Entrar</button>	</center>
		</section>		
	</div>

	<div id="registro" style="display:none;">
		<section id="registro" class="card" style="min-height: 50%;">
			<img src="Recursos/Caballo_vectorized_color.png" alt="" srcset="">
			<form>				
				<div class="input-field col s6">
					<i class="material-icons prefix">account_circle</i>
					<input type="text" id="Rusername" name="Rusername">
					<label for="Rusername">Alias</label>
				</div>
				<div class="input-field col s6">
					<i class="material-icons prefix">vpn_key</i>
					<input type="password" id="Rpass" name="Rpass">
					<label for="Rpass">Contraseña</label>
				</div>			
				<div class="input-field col s6">
					<i class="material-icons prefix">vpn_key</i>
					<input type="password" id="Rpass2" name="Rpass2">
					<label for="Rpass2">Repetir Contraseña</label>
				</div>		
			</form>
			<br>
			¿Ya tienes una cuenta? <a href="#" onClick="iniciarS()">Iniciar Sesión</a>
			<br>
			<center><button onclick="registrarCuenta()" class="waves-effect waves-light btn purple">Registrarse</button>	</center>
		</section>
	</div>

	<div id="app" style="display:none;">
		<div class="navbar-fixed">
		<nav style="background-color:#591876;" id="nav">
		    <div class="nav-wrapper">
		      <a href="#!" class="brand-logo">Sé libre</a>
		      <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
		    </div>
	    </nav>
		</div>	

	    <ul class="sidenav" id="mobile-demo">
			<div id="menuImg"></div>
		    <li><a href="#" onclick="Menu(1);"><i class="small material-icons">perm_identity</i> Red de apoyo</a></li>
		    <li><a href="#" onclick="Menu(2);"><i class="small material-icons">attach_file</i> Marco legal</a></li>
		    <li><a href="#" onclick="Menu(3);"><i class="small material-icons">chrome_reader_mode</i> Directorio</a></li>
			<li><a href="#" onclick="Menu(4);"><i class="small material-icons">favorite</i> Ayuda psicólogica</a></li>
			<li><a href="#" onclick="Menu(6);"><i class="small material-icons">book</i>Cursos</a></li>
		    <li><a href="#" onclick="Menu(5);"><i class="small material-icons">exit_to_app</i>Cerrar sesión</a></li>
	    </ul>

	    <div id="canvas">
	    	
	    	<div id="preguntas" style="display:none;" class="container">
				<br>
				Antes de comenzas, ¿podrías responder las siguientes preguntas?
				<br>
				Mi pareja:
				<br>
				1. Controla continuamente mi tiempo
				<p>
					<label>
						<input name="preg1" type="radio" value="1"/>
						<span>Si</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg1" type="radio" value="2"/>
						<span>No</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg1" type="radio" value="3"/>
						<span>A veces</span>
					</label>
				</p>
				<br>
				2. Es celoso y posesivo
				<p>
					<label>
						<input name="preg2" type="radio" value="1"/>
						<span>Si</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg2" type="radio" value="2"/>
						<span>No</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg2" type="radio" value="3"/>
						<span>A veces</span>
					</label>
				</p>
				<br>
				3. Me acusa de ser infiel y coquetear.
				<p>
					<label>
						<input name="preg3" type="radio" value="1"/>
						<span>Si</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg3" type="radio" value="2"/>
						<span>No</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg3" type="radio" value="3"/>
						<span>A veces</span>
					</label>
				</p>
				<br>
				4. Me desanima constantemente a salir o mantener relaciones con los/as amigos/as y con la familia.
				<p>
					<label>
						<input name="preg4" type="radio" value="1"/>
						<span>Si</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg4" type="radio" value="2"/>
						<span>No</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg4" type="radio" value="3"/>
						<span>A veces</span>
					</label>
				</p>
				<br>
				5. No quiere que estudie.
				<p>
					<label>
						<input name="preg5" type="radio" value="1"/>
						<span>Si</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg5" type="radio" value="2"/>
						<span>No</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg5" type="radio" value="3"/>
						<span>A veces</span>
					</label>
				</p>
				<br>
				6. No quiere que trabaje.
				<p>
					<label>
						<input name="preg6" type="radio" value="1"/>
						<span>Si</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg6" type="radio" value="2"/>
						<span>No</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg6" type="radio" value="3"/>
						<span>A veces</span>
					</label>
				</p>
				<br>
				7. Controla mis gastos y me obliga a rendir cuentas.
				<p>
					<label>
						<input name="preg7" type="radio" value="1"/>
						<span>Si</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg7" type="radio" value="2"/>
						<span>No</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg7" type="radio" value="3"/>
						<span>A veces</span>
					</label>
				</p>
				<br>
				8. Me humilla frente a los demás.
				<p>
					<label>
						<input name="preg8" type="radio" value="1"/>
						<span>Si</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg8" type="radio" value="2"/>
						<span>No</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg8" type="radio" value="3"/>
						<span>A veces</span>
					</label>
				</p>
				<br>
				9. Rompe y destruye objetos de valor sentimental.
				<p>
					<label>
						<input name="preg9" type="radio" value="1"/>
						<span>Si</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg9" type="radio" value="2"/>
						<span>No</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg9" type="radio" value="3"/>
						<span>A veces</span>
					</label>
				</p>
				<br>
				10. Me amenaza.
				<p>
					<label>
						<input name="preg10" type="radio" value="1"/>
						<span>Si</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg10" type="radio" value="2"/>
						<span>No</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg10" type="radio" value="3"/>
						<span>A veces</span>
					</label>
				</p>
				<br>
				11. Me arremete.
				<p>
					<label>
						<input name="preg11" type="radio" value="1"/>
						<span>Si</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg11" type="radio" value="2"/>
						<span>No</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg11" type="radio" value="3"/>
						<span>A veces</span>
					</label>
				</p>
				<br>
				12. Me fuerza a mantener relaciones sexuales.
				<p>
					<label>
						<input name="preg12" type="radio" value="1"/>
						<span>Si</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg12" type="radio" value="2"/>
						<span>No</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg12" type="radio" value="3"/>
						<span>A veces</span>
					</label>
				</p>
				<br>
				13. Arremete a los animales de compañía.
				<p>
					<label>
						<input name="preg13" type="radio" value="1"/>
						<span>Si</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg13" type="radio" value="2"/>
						<span>No</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg13" type="radio" value="3"/>
						<span>A veces</span>
					</label>
				</p>
				<br>
				14. Me desautoriza delante de los/as hijos/as.
				<p>
					<label>
						<input name="preg14" type="radio" value="1"/>
						<span>Si</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg14" type="radio" value="2"/>
						<span>No</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg14" type="radio" value="3"/>
						<span>A veces</span>
					</label>
				</p>
				<br>
				15. Compite con los/as hijos/as por mi atención.
				<p>
					<label>
						<input name="preg15" type="radio" value="1"/>
						<span>Si</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg15" type="radio" value="2"/>
						<span>No</span>
					</label>
				</p>
				<p>
					<label>
						<input name="preg15" type="radio" value="3"/>
						<span>A veces</span>
					</label>
				</p>
				<br>
				<center>
					<a class="waves-effect waves-light btn purple" onClick="examen()"><i class="material-icons right">send</i>Enviar respuestas</a>
				</center>				
	    	</div>

	    	<section id="red" style="display:none;"></section>

			<section id="response" style="display:none;"></section>

	    	<div id="marco" class="container" style="display:none;">
				<p class="title">Marco Legal</p><hr id="title">
				<br>
	    		<br>
	    		Sabemos que tienes muchas preguntas sobre la ley, nosotros te ayudaremos a resolverlas. Tambien es posible que se te dificulte leer la ley, por eso la hemos resumido de una forma fácil de entender, en todo caso podras descargarla.
	    		<br><br>
	    		<center><a class="waves-effect waves-light btn purple" href="ley.pdf"><i class="material-icons right">cloud_download</i>Ley de Igualdad</a></center>
	    		<br><br>
	    		<ul class="collapsible popout">

				    <li>
				      <div class="collapsible-header"><i class="material-icons">description</i>Leer la ley de forma resumida</div>
				      <div class="collapsible-body">
				      	1. Todas las personas son iguales ante la ley, tienen equidad de derechos para poder lograr establece una sociedad democrática que pueda contener una sana convivencia.
			    		<br>
			    		2. Se deben eliminar las barreras que impiden que hombres y mujeres alcancen sus capacidades.
			    		<br>
			    		3. Se deben involucrar todos los ambitos de la vida social para el ejercicio de la presente ley.
			    		<br>
			    		4. Los objetivos de la ley son eliminación de comportamientos indebidos y luchar por la igualdad.
			    		<br>
			    		5. La ley se fundamenta en igual, equidad, respeto y oportunidades tanto para hombres como para mujeres.
			    		<br>
			    		6. Los ciudadanos deben recibir proteción sobre sus derechos civiles por parte de las instituciones.
			    		<br>
			    		7. El Organismo Rector para promover y apoyar la implementación de la presente ley en todas las instancias del Estado, es el Instituto Salvadoreño para el Desarrollo de la Mujer, en adelante ISDEMU, que deberá adecuar sus funciones para garantizar el cumplimiento de la misma.
			    		<br>
			    		8. Son funciones de ISDEMU elaborar un plan  de plan, evaluar, proponer y colaborar en respetar los derechos sobre las personas.
			    		<br>
			    		9. En cumplimiento de compromisos regionales e internacionales contraídos por el Estado en materia de políticas de igualdad y erradicación de la discriminación.
			    		<br>
			    		10. La perspectiva conceptual, metodológica y técnica del enfoque de género, cuyo objetivo es descubrir y reconocer las desigualdades.
			    		<br>
			    		11. El ISDEMU, como organismo rector, velará por el cumplimiento de la aplicación de la estrategia de transversalidad de los Principios Rectores de esta ley.
			    		<br>
			    		12. El gobierno, a través del ISDEMU, aprobará en cada período administrativo, un Plan Nacional que exprese y desarrolle de manera global y coordinada.
			    		<br>
			    		13. La presente ley establece que la elaboración de los presupuestos con enfoque de género, deberán tener en cuenta las diferentes necesidades de mujeres y hombres.
			    		<br>
			    		14. Las instituciones del Estado deberán desagregar por sexo la recopilación, análisis y publicación de toda la información estadística relevante para el objeto y mandatos de esta ley.
			    		<br>
			    		15. A los efectos de esta ley, se entenderá por uso no sexista del lenguaje la utilización de aquellas expresiones lingüísticamente correctas sustitutivas de aquellas que, aun siendo correctas o no, ocultan lo femenino o lo sitúan en un plano secundario o de menor valor respecto al masculino.
			    		<br>
			    		16. El Estado adopta la transversalización del principio constitucional de la igualdad y del principio de no discriminación de las personas por razones de sexo como objetivo fundamental a lograr en todas las actividades educativas dentro de sus competencias.
			    		<br>
			    		17. El propósito de la educación para la igualdad y no discriminación de mujeres y hombres.
			    		<br>
			    		18. El gobierno, a través del ISDEMU y del Ministerio de Educación, fomentará, sin vulnerar la autonomía y libertad establecida en la Ley de Educación Superior o las instituciones de Educación Superior.
			    		<br>
			    		19. Le compete al estado garantizar la igualdad y no discriminación de géneros en las actividades correspondientes.
			    		<br>
			    		20. Se promoverán la igualdad de mujeres y hombres en el ejercicio de los derechos políticos, incluidos entre otros, los derechos al voto, la elegibilidad, el acceso a todas las instancias y niveles de toma de decisiones, así como la libertad de organización, participación y demás garantías civiles y políticas.
			    		<br>
			    		21. Los partidos políticos legalmente establecidos, a fin de garantizar la mayor participación democrátican los procesos de elección de sus autoridades y de candidatos y candidatas.
			    		<br>
			    		22. Se fomentará en los procesos electorales la participación política de la mujer en igualdad de oportunidades entre mujeres y hombres, la equidad de género en el país y sobre las políticas públicas que las garantizan.
			    		<br>
			    		23. Todas las instituciones y organizaciones acreditadas por el gobierno deberán promover y sensibilizar la participación en igualdad de oportunidades de mujeres y hombres en las posiciones y en los procesos de toma de decisiones, en especial en sus órganos de dirección. 
			    		<br>
			    		24. El gobierno garantizará la igualdad y no discriminación de las mujeres en su participación económica.
			    		<br>
			    		25. El Estado a través de las instancias correspondientes, definirá y ejecutará políticas dirigidas a prevenir y erradicar el acoso sexual, acoso laboral y otros acosos generados en las relaciones laborales, en los diferentes regímenes de aplicación.
			    		<br>
			    		26. El Estado adoptará la transversalización del principio constitucional de la igualdad y la no discriminación entre mujeres y hombres como objetivo fundamental a lograr en todas las actividades vinculadas a las responsabilidades públicas en salud.
	    		<br> 
				      </div>
				    </li>

				    <li>
				      <div class="collapsible-header"><i class="material-icons">help</i>Preguntas que seguramente te has hecho</div>
				      <div class="collapsible-body">
				      	<b>¿Qué es violencia Intrafamiliar?</b><br>
				      	Violencia intrafamiliar es el maltrato físico, psíquico, sexual que se da entre miembros de la familia, es aquella que se presenta entre cónyuges o compañeros permanentes; padre y madre de familia, aunque no convivan en un mismo lugar; ascendientes o descendientes de los anteriores y los hijos adoptivos; todas las demás personas que de manera permanente se hallaren integrados a la unidad doméstica<br><br>
				      	<b>Cuando una mujer es víctima de violencia intrafamiliar y quiere presentar una denuncia contra su agresor, ¿ante qué autoridades puede acudir?</b>
				      	Puede acudir a ISDEMU directamente para recibir ayuda sobre su problematica, en todo caso tambien a la PNC o utilizando la linea telefonica 126.<br><br>
				      	<b>¿Existe algún tipo de protección para las víctimas de violencia intrafamiliar y de violencia doméstica (incluyendo a las víctimas de violación)?</b><br>
				      	Si existe, ese tipo de protección, primero se ordena el desalojo del agresos de la victima de la vivienda, el agresosr debe abstenerse de acercarse a la victima.
				      	<br><br>
				      	<b>¿Una mujer esta obligada a tener relaciones con su esposo, si ella no quiere?</b><br>
				      	No esta obligada a tener relaciones, en todo caso de que haya sido obligada se convierte en un caso de violación.
				      </div>
				    </li>

				    <li>
				      <div class="collapsible-header"><i class="material-icons">favorite</i>Consejos</div>
				      <div class="collapsible-body"><span>
				      	<div class="col s12 m5">

					     <div class="card-panel purple darken-2">
					        <span class="white-text">
					        	 No corras riesgos: La primera norma a seguir es la de la protección de la propia vida, la integridad física y seguridad y la del resto de miembros de la familia.
					        </span>
					      </div>

					     <div class="card-panel purple darken-2">
					        <span class="white-text">
					        	 Protégete. Ten previsto un espacio seguro dentro de tu propia vivienda, próximo a un teléfono desde el cual avisar al 911 en caso de urgencia.
					        </span>
					      </div>

					     <div class="card-panel purple darken-2">
					        <span class="white-text">
					        	 No te sientas culpable. Ten muy presente que tú no eres la culpable de estar sufriendo violencia. Es responsable quien la ejerce.
					        </span>
					      </div>

					      <div class="card-panel purple darken-2">
					        <span class="white-text">
					        	 No dejes pasar el tiempo. No pienses que el tiempo lo resolverá. Actúa: el tiempo juega en tu contra y la violencia crecerá cada día.
					        </span>
					      </div>

					      <div class="card-panel purple darken-2">
					        <span class="white-text">
					        	 Protege a los tuyos. Los hijos e hijas, espectadores de la violencia y que la padecen en primera persona, no lo son de manera pasiva. La vivencia diaria de la violencia se instala en el psiquismo de los menores y jóvenes y provoca un sufrimiento con efectos negativos en su desarrollo evolutivo.
					        </span>
					      </div>

				      </div></span></div>
				    </li>

				 </ul>
	    	</div>

	    	<section id="directorio" class="container" style="display:none;"></section>

	    	<div id="ayuda" class="container" style="display:none;"></div>
			
			<div id="detalleBlog" style="display:none;"></div>

			<div id="newTestim" style="display:none;">
				<p class="title">
					Nuevo testimonio
				</p>
				<hr id="title">
				<br>
				<p class="text">
					Comparte tu testimonio con las demás mujeres, puedes recibir ayuda de ellas y apoyarlas
					<br>
					<div class="input-field col s11" style="width: 90%; margin: auto;">
						<i class="material-icons prefix">mode_edit</i>
						<textarea id="testimonioN" class="materialize-textarea" data-length="1000" maxlength="1000" placeholder="Agregar testimonio"></textarea>					
					</div>
					<br>
					<center>
						<a class="waves-effect waves-light btn purple" onClick="enviarTestimonio()"><i class="material-icons right">message</i>Agregar testimonio</a>
						<a class="waves-effect waves-light btn purple" onClick="exitT()"><i class="material-icons right">delete</i>Cancelar</a>
					</center>
				</p>		
				<br>
			</div>

			<div id="newResponse" style="display:none;">
				<p class="title">
					Agregar comentario
				</p>
				<hr id="title">
				<br>
				<p class="text">
					<br>
					Con tu comentario puedes ayudar y apoyar a otras mujeres a romper el círculo de violencia en el que se encuentrán sometidas
					<div class="input-field col s11" style="width: 90%; margin: auto;">
						<i class="material-icons prefix">message</i>
						<textarea id="respuestaN" class="materialize-textarea" data-length="1000" maxlength="1000" placeholder="Agregar comentario"></textarea>					
					</div>
					<br>
					<center>
						<a class="waves-effect waves-light btn purple" onClick="enviarRespuesta()"><i class="material-icons right">message</i>Agregar comentario</a>
						<a class="waves-effect waves-light btn purple" onClick="exitR()"><i class="material-icons right">delete</i>Cancelar</a>
					</center>
				</p>		
				<br>
			</div>

			<div id="cursos" style="display:none;" class="container">
				<p class="title">Cursos</p><hr id="title">
				<br>
	    		<br>
	    		Si no posees un medio de ingresos económicos propios, te ofrecemos esta serie de cursos con los cuales puedes aprender diferentes oficios y profesiones, de esa forma podrás adquirir el conocimiento necesario para sostenerte a ti y a tus hijos económicamente al no estas tu pareja
				<br><br>
				<center>
					<a href="https://www.youtube.com/watch?v=qMtgNaNeQ6Y" target="_blank">
						<section class="post card" style="background-image: url('https://i.pinimg.com/originals/9d/66/8e/9d668ed7966e4f0c629dc76bc5c27fef.jpg');">
							<div class="blog">
								<b>Curso de Panadería Artesanal</b>
								<br>
							</div>
						</section>
					</a>				
					<a href="https://www.youtube.com/watch?v=jCbMQsRq0CI" target="_blank">
						<section class="post card" style="background-image: url('https://i.ytimg.com/vi/jCbMQsRq0CI/maxresdefault.jpg');">
							<div class="blog">
								<b>Curso de Costura</b>
								<br>
							</div>
						</section>
					</a>				
					<a href="https://www.youtube.com/watch?v=jV_K_h1QWwM" target="_blank">
						<section class="post card" style="background-image: url('https://sc01.alicdn.com/kf/HTB10cjTKXXXXXXOXFXXq6xXFXXXz/Artisan-And-Bisuteria-Ecologica-Jewelry-Set.jpg');">
							<div class="blog">
								<b>Curso de Bisutería</b>
								<br>
							</div>
						</section>
					</a>				
					<a href="https://www.youtube.com/watch?v=PJif5o9zAks" target="_blank">
						<section class="post card" style="background-image: url('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ_uF14Mufu17c2s6uhU2D2APtu1MEp51ovWHTSdA1qWYehyb-v');">
							<div class="blog">
								<b>Curso de Fotografía</b>
								<br>
							</div>
						</section>
					</a>				
				</center>
			</div>

			<br>
	    </div>
	</div>

	<div id="newResponse" class="modal">
		<p class="title">
			Agregar comentario
		</p>
		<hr id="title">
		<br>
		<p class="text">
			<br>
			Con tu comentario puedes ayudar y apoyar a otras mujeres a romper el círculo de violencia en el que se encuentrán sometidas
			<div class="input-field col s11" style="width: 90%; margin: auto;">
				<i class="material-icons prefix">message</i>
				<textarea id="respuestaN" class="materialize-textarea" data-length="1000" maxlength="1000" placeholder="Agregar comentario"></textarea>					
			</div>
			<br>
			<center>
				<a class="waves-effect waves-light btn purple" onClick="enviarRespuesta()"><i class="material-icons right">message</i>Agregar comentario</a>
			</center>
		</p>		
		<br>
  	</div>

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/materialize.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">

	//Funciones del sistema

	function Ocultar() {
		$("#preloader").hide();
		$("#bienvenida").hide();
		$("#app").hide();
		$("#login").hide();
		$("#registro").hide();		
	}

	function registro() {
		$("#login").hide();
		$("#registro").show();		
	}

	function iniciarS() {
		$("#login").show();
		$("#registro").hide();		
	}

	function OnStart() {
		//Quitar preloader
		Ocultar();

		//Verificar si primera vez
		var bienvenida = localStorage.getItem("bienvenida");
		if (bienvenida == null) {
			//Primera vez
			$("#bienvenida").show();
			localStorage.setItem("bienvenida", 0);
		} else {
			//Verificar login
			var login = localStorage.getItem("login");
			if (login == null || login == 0) {
				//Poner login
				$("#login").show();
			} else {
				OnApp();
				$("#app").show();
			}
		}
	}

	function Login() {
		var usuario = $("#Lusername").val();
		var pass = $("#Lpass").val();

		var formData = new FormData();
        formData.append("usuario", usuario);
        formData.append("pass", pass);
        $.ajax({
            method: 'POST',
            data: formData,
            url: 'backend/login.php',
            contentType: false,
            cache: false,
            processData: false,
            success:function(resultado)
                {
					var dat = resultado.split('*');					
                	switch(dat["0"]) {
                		case "1":
                			//No existe
							swal("Error", "El usuario que has ingresado no existe", "error");
                			break;
                		case "2":
                			//Contraseña incorrecta
							swal("Error", "Usuario y/o contraseña incorrectas", "error");
                			break;
                		case "3":
							swal("Bienvenida", "", "success");	
                			localStorage.setItem("login", 1);
							localStorage.setItem("user", dat['1']);
							$("#nav").css({"display": "block"});
                			Ocultar();
                			OnApp();
                			$("#app").show();
                			//Exito
                			break;
                	}
                }
        })

	}

	function registrarCuenta() {
		var usuario = $("#Rusername").val();
		var pass = $("#Rpass").val();

		var formData = new FormData();
		formData.append("usuario", usuario);
		formData.append("pass", pass);
		$.ajax({
			method: 'POST',
			data: formData,
			url: 'backend/registro.php',
			contentType: false,
			cache: false,
			processData: false,
			success:function(resultado)
				{
					switch(resultado) {
						case "1":
							//No existe
							swal("Error", "El usuario que has ingresado ya existe", "error");
							break;
						case "2":
							swal("Bienvenida", "", "success");
							localStorage.setItem("login", 1);
							Ocultar();
							OnApp();
							$("#app").show();
							//Exito
							break;
					}
				}
		});

	}

	//Scripts dentro de la App


	function OnApp() {
		$("#red").hide();
		$("#response").hide();
		document.getElementById("cuerpo").classList.remove("fondo");
		var formulario = localStorage.getItem("formulario");
		if (formulario == null) {
			$("#preguntas").show();			
			localStorage.setItem("formulario", 1);			
		} else {
			document.getElementById("red").innerHTML = '<p class="title">Red de apoyo</p><hr id="title"><div class="fixed-action-btn" onClick="newT();"><a class="btn-floating btn-large theme" href="#newTestim"><i class="large material-icons">add</i></a></div><p class="texto">Bienvenida a nuestra red de apoyo, aquí encontrarás testimonios de mujeres que han sido víctimas de la violencia contra la mujer. Juntas, pueden romper el círculo de violencia.</p><div id="testimonios"></div>';
			Redapoyo();
			$("#red").show();			
		}
	}

	function Ocultar2() {
		$("#red").hide();
		$("#marco").hide();
		$("#directorio").hide();
		$("#ayuda").hide();
		$("#preguntas").hide();
	}

	function newT() {
		$("#red").hide();
		$("#newTestim").show();
	}

	function newR() {
		$("#response").hide();
		$("#newResponse").show();
	}

	function Menu(indice) {
		Ocultar2();
		$("#detalleBlog").hide();
		$("#newTestim").hide();
		$("#newResponse").hide();
		$("#cursos").hide();
		switch (indice) {
			case 1:
				$("#red").show();
				$("#response").hide();
				//document.getElementById("red").innerHTML = '<p class="title">Red de apoyo</p><hr><div class="fixed-action-btn"><a class="btn-floating btn-large theme ><i class="large material-icons">add</i></a></div><p class="texto">Bienvenida a nuestra red de apoyo, aquí encontrarás testimonios de mujeres que han sido víctimas de la violencia contra la mujer. Juntas, pueden romper el círculo de violencia.</p><br><div class="input-field col s12"><textarea id="testimonioText" class="materialize-textarea"></textarea><label for="testimonioText">Puedes compartir tu testimonio aquí</label><a id="btn" onClick="enviarTestimonio()" class="waves-effect waves-light btn purple"><i class="material-icons right">send</i></a></div><div id="testimonios"></div>';
				//$("#testimonios")[0].insertAdjacentHTML('afterend', '');
				document.getElementById("red").innerHTML = '<p class="title">Red de apoyo</p><hr id="title"><div class="fixed-action-btn" onClick="newT();"><a class="btn-floating btn-large theme" href="#newTestim"><i class="large material-icons">add</i></a></div><p class="texto">Bienvenida a nuestra red de apoyo, aquí encontrarás testimonios de mujeres que han sido víctimas de la violencia contra la mujer. Juntas, pueden romper el círculo de violencia.</p><div id="testimonios"></div>';
				Redapoyo();
				break;
			case 2:
				$("#marco").show();
				$("#response").hide();
				break;
			case 3:
				$("#directorio").show();
				$("#response").hide();
				document.getElementById("directorio").innerHTML = '<p class="title">Directorio</p><hr id="title"><br><br>En esta sección, puedes encontrar instituciones que pueden ayudarte, recuerda que tú eres muy importante, piensa en tu familia e hijos.<br><br><br><div id="numeros"></div>';
				Instituciones();
				break;
			case 4:
				blog();
				$("#ayuda").show();
				$("#response").hide();
				break;
			case 5:
				//Cerrar sesion
				Ocultar2();
				$("#login").show();
				$("#response").hide();
				localStorage.setItem("login", 0);
				document.getElementById("cuerpo").classList.add("fondo");
				$("#nav").css({"display": "none"});
				break;
			case 6:
				$("#cursos").show();
				$("#response").hide();
				break;
		}
		$('.sidenav').sidenav('close');
	}

	function Instituciones() {
		$.ajax({
			url: 'backend/instituciones.php',
			success: function(respuesta) {
				//Ejecutar
				institucionesT = respuesta.split("*");	
				institucionesT.forEach(function(institucion) {
				 	institucionT = institucion.split("%");
					$("#numeros")[0].insertAdjacentHTML('afterend', '<div class="card instituto"><div class="content"><div class="linea"></div><b>'+institucionT[1]+'</b><br>'+institucionT[4]+'<br><a href="tel:'+institucionT[2]+'">'+institucionT[2]+'</a><br></div></div>');
					$("div.linea").css({"height": ($("div.content").height() + 25) + "px"});
				});
			},
			error: function() {
		        //Error
		    }
		});
	}

	function enviarTestimonio() {
		var testimonio = document.getElementById('testimonioN').value;
		var usuario = localStorage.getItem("user");
		
		if (testimonio != "") {
			var formData = new FormData();
			formData.append("testim", testimonio);
			formData.append("usuario", usuario);
			$.ajax({
				method: 'POST',
				url: 'backend/testim.php',
				cache: false,
				processData: false,
				contentType: false,
				data: formData,
				success:function(result) {
					console.log(result);
					var valor = result.split("*");
					if (valor['0'] == "1") {
						$("#testimonios")[0].insertAdjacentHTML('afterend', '<div class="containertT" onClick="detalleRed('+valor["3"]+')"><div class="testim"><img class="icono" src="https://www.starksein-akademie.de/wp-content/uploads/2018/01/jane-doe.jpg"><p><label class="nombre"><b>'+valor['1']+'</b><br></label>'+testimonio+'<br><label style="color: gray;">'+valor['2']+'</label></p><br><br></div></div>');
						swal("Testimonio publicado", "Tu testimonio ha sido compartido con la comunidad", "success");
						exitT();
					} else {
						swal("Ocurrió algo inesperado", "Favor, reintentarlo en otro momento", "error");
					}
				}
			})
		} else {
			swal("Campos vacíos", "Favor, llenar el campo para continuar", "error");						
		}
	}

	function Redapoyo() {
		$.ajax({
			method: 'POST',
			url: 'backend/red.php',
			cache: false,
			processData: false,
			contentType: false,
			success:function(result) {
				console.log(result);
				testiomonioS = result.split("%");
				testiomonioS.forEach(function(testim) {
					tesT = testim.split("*");
					if (tesT[1] != null) {
						$("#testimonios")[0].insertAdjacentHTML('afterend', '<div class="containertT"><div class="testim" onClick="detalleRed('+tesT["3"]+')"><img class="icono" src="https://www.starksein-akademie.de/wp-content/uploads/2018/01/jane-doe.jpg"><p><label class="nombre"><b>'+tesT[0]+'</b><br></label>'+tesT[1]+'<br><label style="color: gray;">'+tesT[2]+'</label></p><br><br></div></div>');
					} else {
						//$("#testimonios")[0].insertAdjacentHTML('afterend', '<div class="card-panel purple white-text" style="width: 90%; margin: auto;">Aún no hay testimonios publicados. ¡Ánimo, sé la primera en publicar uno! </div>');
					}
				});				
			}
		})		
	}

	function enviarRespuesta() {
		var respuesta = document.getElementById('respuestaN').value;
		var idTestimonio = document.getElementById('testimonioId').value;
		var usuario = localStorage.getItem("user");
		
		if (respuesta != "") {
			var formData = new FormData();
			formData.append("respuesta", respuesta);
			formData.append("usuario", usuario);
			formData.append("idTestimonio", idTestimonio);
			$.ajax({
				method: 'POST',
				url: 'backend/respuesta.php',
				cache: false,
				processData: false,
				contentType: false,
				data: formData,
				success:function(result) {
					console.log(result);
					var valor = result.split("*");
					if (valor['0'] == "1") {
						//$("#testimonios")[0].insertAdjacentHTML('afterend', '<div class="containertT" onClick="detalleRed('+valor["3"]+')"><div class="testim"><img class="icono" src="https://www.starksein-akademie.de/wp-content/uploads/2018/01/jane-doe.jpg"><p>'+testimonio+'<br><label><b>Publicado por: '+valor['1']+' - '+valor['2']+'</b><br></label></p><br><br><hr class="separador"></div></div>');
						$("#respuestas")[0].insertAdjacentHTML('afterend', '<div class="containertT"><div class="testim"><img class="icono" src="https://www.starksein-akademie.de/wp-content/uploads/2018/01/jane-doe.jpg"><p><label class="nombre"><b>'+valor['1']+'</b><br></label>'+valor['3']+'<br><label style="color: gray;">'+valor['2']+'</label></p><br><br></div></div>');
						swal("Comentario publicado", "Tu comentario ha sido compartido con la comunidad", "success");
						exitR();
					} else {
						swal("Ocurrió algo inesperado", "Favor, reintentarlo en otro momento", "error");
					}
				}
			})
		} else {
			swal("Campos vacíos", "Favor, llenar el campo para continuar", "error");						
		}
	}

	function detalleRed(idTestim) {
		$("#red").hide();
		var formData = new FormData();
		formData.append("idTestim", idTestim);
		$.ajax({
			method: 'POST',
			url: 'backend/detalleTestim.php',
			cache: false,
			processData: false,
			contentType: false,
			data: formData,
			success:function(result) {
				console.log(result);
				var testimonioO = result.split("/");				
				var testimonioT = testimonioO['0'].split("*");
				document.getElementById("response").innerHTML = '<input type="text" style="display: none;" value="'+testimonioT['2']+'" id="testimonioId"><p class="title">Red de apoyo</p><hr id="title"><div class="fixed-action-btn"><a class="btn-floating btn-large theme" onClick="newR();"><i class="large material-icons">message</i></a></div><br><div class="containertT"><div class="testim" style="border: none;"><img class="icono" src="https://www.starksein-akademie.de/wp-content/uploads/2018/01/jane-doe.jpg"><p style="font-weight: 725;"><label><b>'+testimonioT['0']+'</b><br></label>'+testimonioT['3']+'<br><label style="color: gray;">'+testimonioT['1']+'</label></p><br></div></div><br><br><br><p class="subtitle">Comentarios de la comunidad<hr class="subtitle"></p><br><div id="respuestas"></div><br><center><a class="waves-effect waves-light btn purple" onClick="OnApp();" style="margin-top: 2%;"><i class="material-icons right">arrow_back</i>Regresar</a></center>';				
				var respuestas = testimonioO['1'].split("%");
				respuestas.forEach(function(rest) {
					respu = rest.split("*");
					$("#respuestas")[0].insertAdjacentHTML('afterend', '<div class="containertT"><div class="testim"><img class="icono" src="https://www.starksein-akademie.de/wp-content/uploads/2018/01/jane-doe.jpg"><p><label><b>'+respu['1']+'</b><br></label>'+respu['0']+'<br><label style="color: gray;">'+respu['2']+'</label></p><br><br></div></div>');
				});				
			}
		})			
		$("#response").show();
	}

	function exitT() {
		$("#red").show();
		$("#newTestim").hide();
	}

	function exitR() {
		$("#response").show();
		$("#newResponse").hide();
	}

	function examen() {
		swal("Analizando respuestas", "Espera un momento por favor, te daremos tus resultados muy pronto", "info");
		var preg1 = $('input:radio[name=preg1]:checked').val();
		var preg2 = $('input:radio[name=preg2]:checked').val();
		var preg3 = $('input:radio[name=preg3]:checked').val();
		var preg4 = $('input:radio[name=preg4]:checked').val();
		var preg5 = $('input:radio[name=preg5]:checked').val();
		var preg6 = $('input:radio[name=preg6]:checked').val();
		var preg7 = $('input:radio[name=preg7]:checked').val();
		var preg8 = $('input:radio[name=preg8]:checked').val();
		var preg9 = $('input:radio[name=preg9]:checked').val();
		var preg10 = $('input:radio[name=preg10]:checked').val();
		var preg11 = $('input:radio[name=preg11]:checked').val();
		var preg12 = $('input:radio[name=preg12]:checked').val();
		var preg13 = $('input:radio[name=preg13]:checked').val();
		var preg14 = $('input:radio[name=preg14]:checked').val();
		var preg15 = $('input:radio[name=preg15]:checked').val();
		var formData = new FormData();
		//ANALISIS
		if (((preg1 == "1") || (preg1 == "3")) && ((preg2 == "1") || (preg2 == "3")) && ((preg3 == "1") || (preg3 == "3"))) {			
			swal("Resultados", "Ten cuidado, te encuentras en un proceso de indicio hacia posibles malos tratos", "info");						
		} else if (((preg4 == "1") || (preg4 == "3")) && ((preg5 == "1") || (preg5 == "3")) && ((preg6 == "1") || (preg6 == "3"))) {			
			swal("Resultados", "Ten cuidado, te encuentras en un proceso dependencia", "info");						
		} else if (((preg6 == "1") || (preg6 == "3")) && ((preg7 == "1") || (preg7 == "3"))) {			
			swal("Resultados", "Ten cuidado, te encuentras en un proceso de control económico", "info");						
		} else if (((preg7 == "1") || (preg7 == "3")) && ((preg8 == "1") || (preg8 == "3")) && ((preg9 == "1") || (preg9 == "3")) && ((preg10 == "1") || (preg10 == "3"))) {			
			swal("Resultados", "Lastimosamente, te encuentras en un proceso de maltrato grave", "info");						
		} else if (((preg12 == "1") || (preg12 == "3"))) {			
			swal("Resultados", "Lastimosamente, sufres de violación", "info");						
		} else if (((preg13 == "1") || (preg13 == "3")) && ((preg14 == "1") || (preg14 == "3")) && ((preg15 == "1") || (preg15 == "3"))) {			
			swal("Resultados", "Te encuentras en situación de maltrato psicológico/emocional", "info");						
		}
		Ocultar2();
		OnApp();
		/*$.ajax({
			method: 'POST',
			url: 'backend/test.php',
			cache: false,
			processData: false,
			contentType: false,
			success:function(result) {
				console.log(result);
				swal("Resultados", result, "info");							
			}
		})*/
	}

	function blog() {		
		document.getElementById('ayuda').innerHTML = "<p class='title'>Ayuda psicológica</p><hr id='title'><br><br>Si estás preguntándote si salir o quedarte, puedes sentirte confundida, asustada y totalmente destrozada, pero debes recordar algo muy importante, no estás sola. Aquí te podemos brindar un apoyo psicológico para que puedas romper todas las barreras y los conflictos a causa del ciclo de la violencia.<br><section id='blog'></section>";
		$.ajax({			
			method: 'POST',
			url: 'backend/blog.php',
			cache: false,
			processData: false,
			contentType: false,
			success:function(result) {
				console.log(result);								
				var blog = result.split("%");				
				blog.forEach(function(rest) {
					respu = rest.split("*");
					$("#blog")[0].insertAdjacentHTML('afterend', '<section class="post card" onClick="detalleBlog('+respu['0']+');" style="background-image: url('+respu['3']+');"><div class="blog"><b>'+respu['1']+'</b><br><label style="color: gray;">'+respu['2']+'</label></div></section>');
				});				
			}
		})			
	}

	function detalleBlog(idBlog) {		
		var formData = new FormData;
		formData.append("idBlog", idBlog);
		$.ajax({			
			method: 'POST',
			url: 'backend/blogD.php',
			cache: false,
			processData: false,
			contentType: false,
			data: formData,
			success:function(result) {
				console.log(result);								
				var blog = result.split("*");				
				document.getElementById('detalleBlog').innerHTML = '<div id="imagen" style="background-image: url('+blog['3']+'); background-size: cover; background-position: center; background-repeat: no-repeat;"></div><p class="title">'+blog['1']+'<br><label style="color: gray;">'+blog['2']+'</label></p><hr id="title"><br><p class="contenido">'+blog['4']+'</p>';
			}
		})			
		$("#ayuda").hide();
		$("#detalleBlog").show();
	}

	//Scripst generales	
	$('.sidenav').sidenav();
	$('.collapsible').collapsible();
	$('.modal').modal();
	$('input#input_text, textarea').characterCounter();
	$('.parallax').parallax();
	
</script>
</body>
</html> 