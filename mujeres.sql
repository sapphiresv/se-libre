-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 19-11-2018 a las 04:57:03
-- Versión del servidor: 10.1.26-MariaDB-0+deb9u1
-- Versión de PHP: 7.0.30-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mujeres`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asistencia`
--

CREATE TABLE `asistencia` (
  `idAsistencia` int(11) NOT NULL,
  `titulo` varchar(125) DEFAULT NULL,
  `contenido` varchar(10000) NOT NULL,
  `fecha` date DEFAULT NULL,
  `imagen` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `asistencia`
--

INSERT INTO `asistencia` (`idAsistencia`, `titulo`, `contenido`, `fecha`, `imagen`) VALUES
(2, 'El ciclo de la violencia en la pareja', 'Lamentablemente, todos conocemos alguna mujer que ha sufrido violencia de género, ya sea en el extremo del maltrato fí­sico o de una manera más sutil (descalificaciones, discriminaciones, acoso verbal, control). Y seguramente los has preguntado alguna vez cómo una mujer puede llegar a esta situación, a tolerarla, a justificar a su agresor y porque no hacer nada para salir de ella.\n<br><br>\n En primer lugar, me gustaría clarificar que la violencia de género es aquella que sufren las mujeres por el hecho de ser mujeres, no solamente en el ámbito de la pareja sino también en el trabajo, en las relaciones con sus iguales, con sus hermanos, en la escuela, en la calle.\n<br><br>\nSin embargo, en este artículo me gustarí­a hablar de la violencia hacia la mujer dentro de las relaciones de pareja, de cómo funciona una relación de maltrato y de porqué es tan difícil huir de ella. \n<br><br>\n- La situación de maltrato en la pareja no se da de forma repentina y aislada sino que es un proceso que se va desarrollando a lo largo del tiempo.\n<br><br>\n- Cuando una mujer sufre maltrato en su relación, hasta se vuelve cada vez más vulnerable, perdiendo con ello su capacidad de autodefensa. \n<br><br>\n En 1979, Leonor Walker, psicóloga estadounidense, desarrolló la teorí­a del ciclo de la violencia, en la que describe las dinámicas de relación violenta en la pareja. Lo describe como un proceso que tiene tres fases que se van sucediendo la una a la otra, es decir, es una secuencia repetitiva, por lo que hace especialmente complicado romper el ciclo. \n<br>\n La frustración al ver que sus esfuerzos no obtienen las consecuencias que espera, y el volver a caer en la situación de maltrato después de las reconciliaciones, provoca en las mujeres la percepción de ser incapaces por ellas mismas de resolver la situación que se atraviesa, llevando este sentimiento de indefensian a una pérdida de autoestima, ansiedad, depresión, insomnio, impotencia, a permanecer aislada por verguenza, por miedo a que no la comprendan, para no sentirse juzgada. \n<br><br>\n Pero recuerda, siempre hay salida a las situaciones de maltrato y que debemos ser conscientes de nuestras dinámicas de relación en la pareja para conseguir romper este ciclo de violencia, y fomentar unas relaciones afectivas sanas en las que las mujeres y hombres respeten su autonomí­a, se perciban con los mismos derechos y asuman sus responsabilidades. ', '2018-11-11', 'http://www.cepfami.com/uploads/pics/pexels-photo-96381.jpg'),
(3, 'Distinguir el maltrato psicológico en la pareja', 'En una relación de pareja se pueden ejercer dos tipos de maltrato: el físico y el psicológico. Respecto al físico, cada vez hay más concienciación en la población general de este tipo de violencia y sus fatales consecuencias, por lo que la condena y el repudio que causa es cada día mayor. Sin embargo, el maltrato psicológico está más “normalizado”, por lo que es más difícil de detectar y evidentemente, actuar frente a ello.\n<br><br>\nNo obstante, en ningún caso se debe de pasar esto por alto, ya que este tipo de agresiones (que son las más frecuentes) anulan a la víctima del maltrato como persona, fruto de un proceso de desvalorización por parte del maltratador. Las consecuencias se ven reflejadas en sentimientos de culpa y soledad, estrés, ansiedad, insomnio, baja autoestima, tristeza, entre muchos otros.\n<br><br>\nPara detectarlo con mayor facilidad, hemos de atender si hay alguno de estos factores de alarma: \n<br><br>\n- Miedo a hablar con con la pareja por temor a su reacción<br>\n- Te aísla (privarte de quedar con tus amistades o familiares)<br> \n- Te culpa de lo malo que le sucede; desprecia tus logros y te descalifica en presencia de otros<br> \n- Te humilla, grita, insulta o amenaza si no haces lo que te pide<br>\n- Controla tus planes y, finalmente, si te da miedo estar en su presencia (te asusta lo que te pueda hacer) es signo de que tu pareja te está maltratando psicológicamente.\n<br><br>\nEn el mundo tecnológico de hoy en día, es frecuente que el maltratador revise tus chats de WhatsApp o tus redes sociales. También es habitual que en público te ridiculice o menosprecie sutilmente y que descargue sus ataques de ira en ti. \n<br><br>\nMuchas veces la culpa o vergüenza de la víctima hace que prefiera pasar por alto este tipo de acciones y al no ponerle freno, la conducta agresiva del maltratador se convierte en algo habitual.\n<br><br>\nPor esa razón es importante conocer las señales de alarma que acabamos de mostrar para establecer unos límites bien y saber a partir de qué punto se está agrediendo a alguien y se están violentando sus derechos.\n<br><br>\nEn este sentido, la terapia se convierte en el espacio necesario para ayudar a detectar estas señales y actuar en consecuencia para poner fin al maltrato. Además, después de haber sufrido esta clase de abuso, será imprescindible un trabajo psicoterapéutico que permita el cambio de un estado apático a un redescubrimiento de uno mismo, de tal forma que conlleve una mejoría en el estado de ánimo y la calidad de vida. Es sin duda una labor que requerirá esfuerzo y paciencia, pero permitirá también recuperar aquellas facetas de uno mismo que se han podido perder por culpa de haber sufrido maltrato psicológico.\n\n\n', '2018-11-08', 'http://www.cepfami.com/uploads/pics/black-and-white-person-woman-girl.jpg'),
(4, '¿Y ahora qué?', 'Cuando una mujer se ve involucrada en el ciclo del maltrato, o cuando ha logrado salir de él, es totalmente comprensible que se plantee esta pregunta y que no encuentre una respuesta inmediata<br><br>\nEs una pregunta que podríamos hacernos tras vivir una situación difícil… Personalmente, me suscita incertidumbre al mismo tiempo que esperanza y ganas de seguir adelante. Cuando una mujer se ve involucrada en el ciclo del maltrato, o cuando ha logrado salir de él, es totalmente comprensible que se plantee esta pregunta y que no encuentre una respuesta inmediata. En un momento tan delicado, tener un espacio para rehacer su vida y volver a encontrar ese equilibrio que tanto anhela, se convierte casi en una necesidad.<br><br>\nCuando hablamos de violencia, es común pensar automáticamente en las agresiones físicas. Pero la violencia también puede ejercerse a nivel psicológico, verbal y sexual. Una vez sanadas las heridas visibles, el malestar que subyace en el individuo es el mental. Hablamos de un malestar tan profundo que escapa de nuestra comprensión.<br><br>\nA modo de poder entenderlo y darle un sentido para poder aliviarlo, se buscan respuestas racionalizadoras; muchas veces se cae en el error de la autoculpabilización. Pero ¿es esta la solución? ¿Es así cómo vamos a conseguir aumentar la autoestima, la autoeficacia y la confianza en una misma y en los demás? ¿Es la culpa la que va a hacer desaparecer el miedo y la impotencia que nos invade?<br><br>\nDejadme que exponga otra perspectiva. Cuando una mujer maltratada decide buscar ayuda, lo suele hacer sola; lo hace pensando en ella y en sus hijos (en el caso de tenerlos), ya que éstos también son víctimas directas del maltrato. Pero no podemos generalizar, ya que en ocasiones el tema del maltrato se desvela estando en terapia de pareja. Es entonces cuando se dan cuenta que tienen sus propias capacidades para poder afrontar la situación. Capacidades que están escondidas porque el miedo no las deja salir. Pero haciéndolas conscientes y empoderándolas, ya se está dando un gran paso para acabar con el ciclo de la violencia.<br><br>\nHablamos de uno pilares que ante un episodio traumático de este tipo se desvanecen. Es elección de una misma volverlos a construir. El terapeuta se convierte en una especie de tutor en resiliencia que ayuda a fortalecer los pilares de la confianza, la autoestima, la identidad, la independencia, la autonomía, la introspección, reflexión y sentido crítico y la capacidad para relacionarse. Todas ellas son capacidades imprescindibles para poder afrontar las adversidades y poder seguir adelante.<br><br>\nPor otro lado, poder elaborar una concepción distinta del problema puede contribuir en la búsqueda de estrategias para superar la situación. Consideremos también que el apoyo familiar y social es de gran importancia en este tipo de casos. Escuchar la voz de todos suele ayudar a comprender la situación desde otra perspectiva. La recuperación del trauma y la proporción de recursos para afrontar la situación, se convierten en objetivos imprescindibles. Y digo esto porque cuando una mujer maltratada buscar ayuda, está pidiendo implícitamente que se le ayude a activar sus factores resilientes porque no sabe cómo hacerlo.<br><br>\nLo que quiero transmitiros después de haber escrito estas líneas, es que sí, es cierto que la violencia de género produce daños en la salud física y psicológica. Y sí, es cierto que el propio maltrato puede dañar muchas de las capacidades resilientes. Pero la resiliencia, o dicho de otra forma, la capacidad para superar las adversidades vitales, es una capacidad que se puede trabajar y potenciar. De esta forma, la mujer se va empoderando, va siendo consciente de la situación y cada vez estará más preparada para poder afrontar las dificultades, hasta que pueda salir al fin de la relación de violencia.', '2018-11-04', 'http://www.cepfami.com/uploads/pics/yahoraquA_c_.jpg'),
(5, '¿Qué me está diciendo la violencia?', 'La violencia es un elemento muy presente en nuestra sociedad: violencia de género, violencia familiar, violencia sexual, moving, peleas, bulling, bandas, violencia institucional.<br><br>\r\nLa violencia es un elemento muy presente en nuestra sociedad: violencia de género, violencia familiar, violencia sexual, moving, peleas, bulling, bandas, violencia institucional… Cuando la violencia nos toca, es difícil escuchar ningún otro mensaje. Cuando se dirige a nosotros o cuando somos testigos, el rechazo, el miedo y la rabia lo llenan todo.<br><br>\r\nEscuchar no es aceptar. El responsable de la violencia es la persona que agrede, pero ¿qué hacemos si quien agrede es niño o adolescente? Si mi hija adolescente tiene conductas de violencia hacia mí, algo le está ocurriendo. La violencia me grita, pero no entiendo qué me está diciendo; me insulta, me humilla, me golpea. Todo esto me impide escuchar qué me está diciendo, pero lo que me provoca, la rabia, el rechazo y la culpa, también me dificultan poner un límite a esta violencia. Puede estar ocurriendo en la escuela o en el seno de la familia, lo que hace que se oculte y se aguante la situación, pero esto no ayuda, ni a la familia, ni al adolescente, que forma parte de esta familia.<br><br>\r\nComprender no es aceptar. Las causas de la violencia son múltiples, y las situaciones en las que la violencia se produce, complejas. Comprender qué está ocurriendo no quiere decir que se esté justificando; tampoco el hecho de comprender qué está ocurriendo significa que se esté haciendo algo para solucionarlo. Lo que hace esto es abrir la puerta a otra conversación porque ponemos a un lado la rabia y la culpa que esto me produce y empezamos a tratar los temas, las emociones y las heridas que están por debajo.  Para esto en muchas ocasiones es necesario pedir ayuda; una ayuda que nos permita ver más allá de la violencia y a romper el bloqueo que la culpa o la vergüenza nos está produciendo.<br><br>\r\nAceptar (a la persona) no es aceptar (la violencia). Para que se le pueda poner un límite al niño o al adolescente, primero éste ha de saber que la aceptación de él o ella como persona es incondicional. Acepto quien eres, pero no lo que haces cuando te enfadas o cuando te frustras o cuando te desbordas. Puedes contar conmigo, podemos dialogar sobre lo que te está ocurriendo, pero no voy a tolerar que me hagas daño. Y esto, que parece una protección de uno mismo, es también la protección hacia al otro a varios niveles; el más directo es que la persona, cuando agrede, también resulta dañada.<br><br>\r\n', '2018-11-14', 'http://www.cepfami.com/uploads/pics/violencia_cepfami_01.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `chatbot`
--

CREATE TABLE `chatbot` (
  `idChat` int(11) NOT NULL,
  `keywords` varchar(250) DEFAULT NULL,
  `respuesta` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `chatbot`
--

INSERT INTO `chatbot` (`idChat`, `keywords`, `respuesta`) VALUES
(1, 'testimonios,mujeres,otras,comentarios,red,apoyo,testimonio,hay', 'Por supuestos, aqui estan una serie de testimonios de mujeres que han sido victimas de este tipo de violencia.'),
(2, 'que,es,se libre,quienes,hacen', 'Se libre es una applicacion informatica que te permite romper el sliencio ante toda forma de violencia contra la mujer.'),
(3, 'como,llamas,nombre,quien,eres', 'Soy Cataleya :), estoy aqui para ayudarte con todas tus dudas'),
(4, 'reglamentos,leyes,reglas,articulos,ley,me,nos,protegen', 'Existen una gran cantidad de leyes y articulos que se aseguran de tu proteccion, entre ellos esta la Ley de Igualdad impulsada por la PGR, que busca la igualdad entre todas las personas y principalmente, garantizar tu seguridad '),
(5, 'institutos,organimos,organizaciones,ayuda,mujer,contra,violencia', 'En El Salvador hay diversas instituciones que velan por la proteccion de las mujeres, entre dichas instituciones se encuentran las siguientes:'),
(6, 'que,poder,hacer,duda', 'consejo'),
(7, 'existen,existencia,redes,apoyo,grupos,ayuda', 'Por supuesto, en nuestra applicacion existe una red de apoyo entre mujeres con quienes puedes compartir testimonios reales acerca de como afrontar esta problematica. Puedes visitarnos en el siguiente enlace: http://selibre.tk');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `institucion`
--

CREATE TABLE `institucion` (
  `idInstitucion` int(11) NOT NULL,
  `nombreInstitucion` varchar(75) DEFAULT NULL,
  `telefono` varchar(9) DEFAULT NULL,
  `correo` varchar(100) DEFAULT NULL,
  `direccion` varchar(125) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `institucion`
--

INSERT INTO `institucion` (`idInstitucion`, `nombreInstitucion`, `telefono`, `correo`, `direccion`) VALUES
(1, 'Instituto Salvadoreño para el Desarrollo de la Mujer', '2510-4100', 'isdemu@isdemu.gob.sv', 'C.A. 9a. Av. Norte #120 San Salvador'),
(2, 'Procuraduria General de la República', '2231-9484', 'info@pgr.gob.sv', 'Novena Calle Poniente y Trece Avenida Norte, Torre PGR, Centro de Gobierno, San Salvador, San Salvador'),
(3, 'Ministerio de justicia y seguridad pública (Atención a victimas)', '2231-9484', 'victimas@mjsp.gob.sv', 'Alameda Juan Pablo II y 17 Av Norte, Complejo Plan Maestro, Edificios B1, B2 y B3, San Salvador, El Salvador, CA'),
(4, 'Policia Nacional Civil', '2527-1000', 'comunicacion.pnc@gmail.com', '6ta. Calle Oriente entre 8va y 10ma Ave. Sur # 42 Barrio La Vega, San Salvador, El Salvador, C.A.'),
(5, 'Ciudad Mujer', '2244-2700', 'atencion@ciudadmujer.gob.sv', 'Calle José Martí, #15, Col.Escalón, Atrás de la Residencia Presidencial, San Salvador, El Salvador, C.A. ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mujeres`
--

CREATE TABLE `mujeres` (
  `idMujer` int(11) NOT NULL,
  `aliasMujer` varchar(10) DEFAULT NULL,
  `resultadoExamen` enum('1','2','3','4','5','6') DEFAULT NULL,
  `passMujer` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `mujeres`
--

INSERT INTO `mujeres` (`idMujer`, `aliasMujer`, `resultadoExamen`, `passMujer`) VALUES
(1, 'rosa', NULL, '123456'),
(2, 'danni', NULL, '123456'),
(3, 'ines', NULL, '123456'),
(4, 'mariana', NULL, '123456'),
(5, 'sofia', NULL, '123456'),
(6, 'ruth', NULL, '123456'),
(7, '', NULL, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestas`
--

CREATE TABLE `respuestas` (
  `idRespuesta` int(11) NOT NULL,
  `idTestimonio` int(11) NOT NULL,
  `idMujer` int(11) DEFAULT NULL,
  `idInstitucion` int(11) DEFAULT NULL,
  `consejo` varchar(1000) DEFAULT NULL,
  `fecha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `respuestas`
--

INSERT INTO `respuestas` (`idRespuesta`, `idTestimonio`, `idMujer`, `idInstitucion`, `consejo`, `fecha`) VALUES
(15, 13, 2, NULL, 'Vaya, Sofia tu historia me ha hecho reflexionar sobre todo lo que estoy viviendo, mi esposo actúa así y no lo soporto más, necesitaba enterarme que hay muchas que han salido de una situación similar a la mía, gracias.', '2018-11-11'),
(16, 13, 3, NULL, 'Te felicito yo también rompí esas cadenas y ahora vivo feliz, mis hijos ya no sufren de violencia intrafamiliar y se que jamás lo harán ya no lo permitiré.', NULL),
(17, 12, 6, NULL, 'Mi hija también fue agredida sexualmente por mi ex esposo y en ningún momento la culpo a ella, sé por el infierno que pasaste y los ratos desagradables que viviste yo la apoye hasta que el pagase por lo que hizo, admiro mucho tu valor y agradezco que a pesar de todo motives a las demás para que no callen estas cosas, todas tenemos derecho a ser libres.', '2018-11-12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `testimonios`
--

CREATE TABLE `testimonios` (
  `idTestimonio` int(11) NOT NULL,
  `idMujer` int(11) DEFAULT NULL,
  `testimonio` varchar(1000) DEFAULT NULL,
  `fechaHora` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `testimonios`
--

INSERT INTO `testimonios` (`idTestimonio`, `idMujer`, `testimonio`, `fechaHora`) VALUES
(9, 3, 'Mi esposo al principio era un caballero con el tiempo, el empezó a exceder con sus celos, me decía que debía usar y que no, me empezó a prohibir salir con mis amigas y familiares, yo me sentía dominada y no quise acceder más a su comportamiento entonces empezó a golpearme y gritarme  mis hijos se asustaron, lloraron. Eso me dolió tanto que decidí obedecer antes que mis hijos vivieran un capítulo igual otra vez temía por mis hijos, hasta que un día ya no se trababa de celos era dominio, era placer, solo me golpeaba sin razón alguna decidí buscar ayuda con instituciones ellos me explicaron que mis hijos no quedarían desamparados y ahora les digo a ustedes que también pueden ser libres.', '2018-11-19'),
(12, 2, 'Mi papá nos abandono a mi mamá y a mi, ella con el tiempo rehizo su vida lo cual me hizo feliz, pero jamás pensé que mi padrastro empezaría a abusar sexualmente de mi, el me amenazaba decía que le diría a mi madre que yo era una \"perra\" que yo lo provocaba, que nos mataría a las dos si yo decía algo, cada noche, era un infierno para mi, cuando mi mamá se iba al trabajo yo era su marioneta, estaba cansada y decidí parar aunque eso nos costará la vida, la dije todo a mi madre, el casi la mata, pero gracias a que conocimos las leyes que nos protegen el ahora está cumpliendo su condena y pagando sus crimines, mi mamá me ama y no me reprocha nada, las dos somos libres tu también puedes serlo, no tengas miedo.', '2018-11-12'),
(13, 5, 'Eso no era amor... Mi ex esposo, me pegaba, me humillaba, me insultaba, me amenazaba, me obligaba a hacer y dejar de hacer cosas y al final decía \"lo hago por que te amo y quiero lo mejor para nosotros\", yo pensé que el tenia razón aguante eso por años, un día lo supe,  supe que el amor no es así, que nadie tiene derecho a verte de menos, que no debía soportar sus humillaciones cuando decidí separarme de él, me golpeó tan fuerte que me fracturó dos costillas, nos separamos y fue la mejor decisión ahora vivo plenamente, sin que nadie se burle de mi cabello, de mi cuerpo, de mis sueños... eso no era amor, ahora ¡soy libre! y se que también tu puedes serlo, cuentas con el apoyo de todas nosotras porque no queremos ni una menos.', '2018-11-06');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `asistencia`
--
ALTER TABLE `asistencia`
  ADD PRIMARY KEY (`idAsistencia`);

--
-- Indices de la tabla `chatbot`
--
ALTER TABLE `chatbot`
  ADD PRIMARY KEY (`idChat`);

--
-- Indices de la tabla `institucion`
--
ALTER TABLE `institucion`
  ADD PRIMARY KEY (`idInstitucion`);

--
-- Indices de la tabla `mujeres`
--
ALTER TABLE `mujeres`
  ADD PRIMARY KEY (`idMujer`);

--
-- Indices de la tabla `respuestas`
--
ALTER TABLE `respuestas`
  ADD PRIMARY KEY (`idRespuesta`),
  ADD KEY `fbMujer` (`idMujer`),
  ADD KEY `fbInstituto` (`idInstitucion`);

--
-- Indices de la tabla `testimonios`
--
ALTER TABLE `testimonios`
  ADD PRIMARY KEY (`idTestimonio`),
  ADD KEY `idMujer` (`idMujer`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `asistencia`
--
ALTER TABLE `asistencia`
  MODIFY `idAsistencia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `chatbot`
--
ALTER TABLE `chatbot`
  MODIFY `idChat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `institucion`
--
ALTER TABLE `institucion`
  MODIFY `idInstitucion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `mujeres`
--
ALTER TABLE `mujeres`
  MODIFY `idMujer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `respuestas`
--
ALTER TABLE `respuestas`
  MODIFY `idRespuesta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT de la tabla `testimonios`
--
ALTER TABLE `testimonios`
  MODIFY `idTestimonio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `respuestas`
--
ALTER TABLE `respuestas`
  ADD CONSTRAINT `fbInstituto` FOREIGN KEY (`idInstitucion`) REFERENCES `institucion` (`idInstitucion`),
  ADD CONSTRAINT `fbMujer` FOREIGN KEY (`idMujer`) REFERENCES `mujeres` (`idMujer`);

--
-- Filtros para la tabla `testimonios`
--
ALTER TABLE `testimonios`
  ADD CONSTRAINT `testimonios_ibfk_1` FOREIGN KEY (`idMujer`) REFERENCES `mujeres` (`idMujer`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
