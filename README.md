# Sé Libre

Una plataforma para combatir la violencia contra las mujeres en los países de El Salvador, Guatemala y Honduras de un forma integral e innovadora. Por medio de las nuevas tecnologías, buscamos reducir los índices de violencia contra la mujer en dichos países y fomentar en las niñas y mujeres de la región una cultura de prevención, darles a ellas el conocimiento de sus derechos, las instituciones que las protegen, las alternativas económicas pero principalmente brindarles el apoyo de otras mujeres que han estado en esa misma situación de violencia, de esa forma ellas pueden romper el círculo de la violencia en el que se encuentran sometidas.

Todo ello lo lograremos gracias a una aplicación progresiva para teléfonos Android, iOS y computadoras, 