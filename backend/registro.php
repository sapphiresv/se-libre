<?php
    require_once("conexion.php");
    session_start();
    $usuario = filter_var($_POST["usuario"], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $pass = filter_var($_POST["pass"], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $sql1 = "SELECT `idMujer` FROM `mujeres` WHERE `aliasMujer` = ?";
    $data1 = array($usuario);
    $resu = Conexion::LLAMAR_FILA($sql1, $data1);
    if ($resu != NULL) {
        echo "1";        
    } else {
        $sql = "INSERT INTO `mujeres`(`aliasMujer`, `passMujer`) VALUES (?, ?)";
        $data = array($usuario, $pass);
        Conexion::EJECUTAR_CONSULTA($sql, $data);
        $sql2 = "SELECT MAX(`idTestimonio`) AS 'id' FROM `testimonios`";        
        $id = Conexion::LLAMAR_FILA($sql2, NULL);
        $_SESSION['idMujer'] = $id['id'];
        echo "2";
    }
    
?>