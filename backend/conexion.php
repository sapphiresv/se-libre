<?php
    class Conexion
    {
    	//VARIABLE PRIVADA
        private static $CONEXION;

        //FUNCION PARA LA CONEXION A LA DB
        private static function CONECTAR() 
        {
            //VARIABLES CON LOS DATOS PARA LA CONEXION
            /*$SERVER = 'localhost';
            $DATABASE = 'mujeres';
            $USUARIO = 'phpmyadmin';
            $CONTRASENIA = 'mujeres2018';*/
            $SERVER = 'localhost';
            $DATABASE = 'mujeres';
            $USUARIO = 'root';
            $CONTRASENIA = '';
            //$OPCIONES = array(PDO::MYSQL_ATTR_INIT_COMMAND => "set names utf8");
            //SE AGREGAR UN VALOR A LA CONEXION
            self::$CONEXION = null;
            //SE USA TRY POR ERRORES
            try
            {
            	//SE EJECUTA LA CONEXION EN PDO
                self::$CONEXION = new PDO("mysql:host=".$SERVER."; dbname=".$DATABASE, $USUARIO, $CONTRASENIA, NULL);
                //
                self::$CONEXION->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            catch(PDOException $EXCEPCION)
            {
                die($EXCEPCION->getMessage());
            }
        }

        //FUNCION PARA DESCONECTAR LA BASE
        private static function DESCONECTAR()
        {
        	//SE CAMBIA EL VALOR DE LA VARIABLE PRIVADA
            self::$CONEXION = null;
        }

        //FUNCION PARA EJECUTAR CONSULTAS SQL RECIBIENDO EL VALOR Y LA CONSULTA
        public static function EJECUTAR_CONSULTA($CONSULTA, $VALOR)
        {
        	try
            {
                //SE EJECUTA LA CONEXION A LA DB
                self::CONECTAR();
                //SE PREPARA LA CONSULTA A EJECUTAR
                $RESULTADO = self::$CONEXION->prepare($CONSULTA);
                //SE EJECUTA LA CONSULTA PASANDO EL VALOR
                $RESULTADO->execute($VALOR);
                //SE DESCONECTA LA DB
                self::DESCONECTAR();         
            }
            catch (Exception $EX)
            {   
                echo $RESULTADO -> errorCode();
            }
        }

        //FUNCION PARA LLAMAR UNA SOLA FILA RECIBIENDO EL VALOR Y LA CONSULTA
        public static function LLAMAR_FILA($CONSULTA, $VALOR)
        {
        	//SE CONECTA LA DB
            self::CONECTAR();
            //SE PREPARA LA CONSULTA A EJECUTAR
            $RESULTADO = self::$CONEXION->prepare($CONSULTA);
            //SE EJECUTA LA CONSULTA PASANDO EL VALOR
            $RESULTADO->execute($VALOR);
            //SE DESCONECTA LA DB
            self::DESCONECTAR();
            //SE DEVUELVE LA FILA OBTENIDA EN UN ARREGLO
            return $RESULTADO->fetch(PDO::FETCH_BOTH);
        }

        //FUNCION PARA LLAMAR MAS DE UNA FILA RECIBIENDO EL VALOR Y LA CONSULTA
        public static function LLAMAR_FILAS($CONSULTA, $VALOR)
        {
        	//SE CONECTA LA DB
            self::CONECTAR();
            //SE PREPARA LA CONSULTA
            $RESULTADO = self::$CONEXION->prepare($CONSULTA);
            //SE EJECUTA LA CONSULTA PASANDO EL VALOR
            $RESULTADO->execute($VALOR);
            //SE DESCONECTA LA DB
            self::DESCONECTAR();
            //SE DEVUELVEN LAS FILAS OBTENIDAS EN UN ARREGLO
            return $RESULTADO->fetchAll(PDO::FETCH_BOTH);
        }
    }
?>