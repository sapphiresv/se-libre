<?php 
    require_once("conexion.php");
    session_start();
    $testim = filter_var($_POST['testim'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $idMujer = filter_var($_POST['usuario'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

    $sql = "INSERT INTO `testimonios`(`idMujer`, `testimonio`, `fechaHora`) VALUES (?, ?, ?)";
    $datos = array($idMujer, $testim, date('Y-m-d'));
    Conexion::EJECUTAR_CONSULTA($sql, $datos);

    $dataSQL = "SELECT `mujeres`.`aliasMujer`, `testimonios`.`fechaHora`, `testimonios`.`idTestimonio` FROM `testimonios`, `mujeres` WHERE `testimonios`.`idMujer` = `mujeres`.`idMujer` AND `testimonios`.`idTestimonio` = (SELECT MAX(`testimonios`.`idTestimonio`) FROM `testimonios`)";
    $resultSQL = Conexion::LLAMAR_FILA($dataSQL, NULL);
    echo "1*" . $resultSQL['aliasMujer'] . "*" . $resultSQL['fechaHora'] . "*" . $resultSQL['idTestimonio'];
?>