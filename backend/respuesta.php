<?php 
    require_once("conexion.php");
    session_start();
    $idTestimonio = filter_var($_POST['idTestimonio'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $idMujer = filter_var($_POST['usuario'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $respuesta = filter_var($_POST['respuesta'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

    $sql = "INSERT INTO `respuestas`(`idTestimonio`, `idMujer`, `consejo`, `fecha`) VALUES (?, ?, ?, ?)";
    $datos = array($idTestimonio, $idMujer, $respuesta, date('Y-m-d'));
    Conexion::EJECUTAR_CONSULTA($sql, $datos);

    $dataSQL = "SELECT `respuestas`.`consejo`, `mujeres`.`aliasMujer`, `respuestas`.`fecha` FROM `respuestas`, `mujeres` WHERE `mujeres`.`idMujer` = `respuestas`.`idMujer` AND `respuestas`.`idRespuesta` = (SELECT MAX(`respuestas`.`idRespuesta`) FROM `respuestas`)";
    $resultSQL = Conexion::LLAMAR_FILA($dataSQL, NULL);
    echo "1*" . $resultSQL['aliasMujer'] . "*" . $resultSQL['fecha'] . "*" . $resultSQL['consejo'];
?>