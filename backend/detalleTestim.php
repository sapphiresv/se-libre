<?php
    require_once("conexion.php");
    session_start();
    $idTestim = filter_var($_POST['idTestim'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $testimSQL = Conexion::LLAMAR_FILA("SELECT `mujeres`.`aliasMujer`, `testimonios`.`fechaHora`, `testimonios`.`idTestimonio`,  `testimonios`.`testimonio` FROM `testimonios`, `mujeres` WHERE `testimonios`.`idMujer` = `mujeres`.`idMujer` AND `testimonios`.`idTestimonio` = ?", array($idTestim));
    $responseT = $testimSQL['0'] . '*' . $testimSQL['1'] . '*' . $testimSQL['2'] . '*' . $testimSQL['3'] . '/';
    $respuestasSQL = Conexion::LLAMAR_FILAS("SELECT `respuestas`.`consejo`, `mujeres`.`aliasMujer`, `respuestas`.`fecha` FROM `respuestas`, `mujeres` WHERE `mujeres`.`idMujer` = `respuestas`.`idMujer` AND `respuestas`.`idTestimonio` = ? ORDER BY `respuestas`.`idRespuesta` ASC", array($idTestim));
    foreach ($respuestasSQL as $respuestaD) {
        $responseT .= $respuestaD['0'] . '*' . $respuestaD['1'] . '*' . $respuestaD['2'] . '%';
    }
    echo substr($responseT, 0, -1);
?>