<?php
    require('conexion.php');
    
    $user = filter_var($_POST['usuario'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
    $pass = filter_var($_POST['pass'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

    $sql = "SELECT `idMujer`, `aliasMujer`, `resultadoExamen`, `passMujer` FROM `mujeres` WHERE `aliasMujer` = ?";
    $data = array($user);
    $result = Conexion::LLAMAR_FILA($sql, $data);

    if ($result != NULL) {
        if ($pass == $result['passMujer']) {
            echo "3*" . $result['idMujer'];
            session_start();
            $_SESSION['idMujer'] = $result['idMujer'];
        } else {
            echo "2";
        }        
    }
    else {
        echo "1";
    }
?>